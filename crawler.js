var fs = require('fs');
var system = require('system');
var webserver = require('webserver');

var Crawler = function () {
    
    var me  = this;        
    
    me.results = [];
    
    var getLogFileName = function (keywords) {
        
        var currentTime = new Date(),
			month = (currentTime.getMonth() + 1).toString(),
			day = currentTime.getDate().toString(),
			year = currentTime.getFullYear();
            
			return '' + year + (month[1]?month:'0'+month[0]) + (day[1]?day:'0'+day[0]) + '_' + keywords +".txt";
    }
    
    me.startCrawling = function (keyword, startFrom, response) {
        
        var CSE = require('./googlecse/cse').create();
        
        var callback = function (result) {
                
            console.log('result received...');
            console.log(JSON.stringify(result));
            response.write(JSON.stringify(result));
            response.statusCode = 200;
            response.contentType = 'text/plain';           
            response.close();
        };
        
        CSE.search(keyword, startFrom, callback);
    }
}

var server = webserver.create();

console.log('listening at 8080');

var service = server.listen(8080, function(request, response) {
  
  console.log('request: ', request.url);
  var queryString = parseGET(request.url);
  
  var crawler = new Crawler();  
  
  if(!queryString.keyword){
    console.log('no keyword, closing...');
    response.close();
    return;
  }
  
  console.log('params: ',queryString.keyword, queryString.pageNumber);
  
  response.contentType = 'text/plain';
  crawler.startCrawling(queryString.keyword, queryString.pageNumber, response);
  
});

function parseGET(url){
  
    var query = url.substr(url.indexOf("?")+1);
    var result = {};
    query.split("&").forEach(function(part) {
        var e = part.indexOf("=")
        var key = part.substr(0, e);
        var value = part.substr(e+1);
        result[key] = decodeURIComponent(value);
    });
  return result;
}