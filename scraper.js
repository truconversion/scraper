var request = require('request');
var Q = require('q');

var Scraper = function () {
    
    var me = this;
    
    var shortUrl = function (url) {
        
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            protocol = url.substr(0, url.indexOf("://") + 3);
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];
        
        domain = domain.trim().replace(/^www./, '');

        return domain.trim();
    }
    
    me.searchInPage = function (url, keyword) {
        
        var deferred = Q.defer(),
            opts = {
                url: url,
                followRedirect: false,
                timeout: 1*60*1000 // 1 minute
            };        
        
        try {
            
           // console.log('opening ', url);
            
            request(opts, function (err, res, body) {
                
                if(err){
                    console.log('scraper error: ', err);
                    deferred.resolve({
                        err: err,
                        success: false,
                        matches: [],
                        emails: ''
                    });
                    return;
                }
            
                var regex = new RegExp(keyword, 'gi'),
                    matches = body.match(regex) || [],
                    emailRegex = new RegExp('[a-z0-9\\-_\\.+]+@'+ shortUrl(url), 'gi');
                    emailAddresses = body.match(emailRegex);
                
                //console.log(matches);
                deferred.resolve({
                    success: true,
                    matches: matches,
                    emails: emailAddresses && emailAddresses.length > 0 ? emailAddresses.unique().join(';') : ''
                });
            })
            .on('error', function (e) {
                deferred.resolve({
                    err: e,
                    success: false,
                    matches: [],
                    emails: ''
                });
            });
            
        } catch (error) {
            deferred.resolve({
                err: error,
                success: false,
                matches: [],
                emails: ''
            });
        }
        
        return deferred.promise;
    }
};

// module registartion
module.exports = {

    create: function () {
        return new Scraper();
    }

}

Array.prototype.unique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
}