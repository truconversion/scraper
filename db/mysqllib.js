var mysql = require("mysql");
var Q = require("q");

var MySqlLib = function () {
    
    var me = this;
        
    runQuery = function (con, sqlQuery, params, callback) {
        
        if(params) 
            con.query(sqlQuery, params, callback);
        else
            con.query(sqlQuery, callback);
    }
    
    var getDbConnection = function () {
        
        var con = mysql.createConnection({
            host: "expodbinstance1.cypqfknbvaix.us-east-1.rds.amazonaws.com",
            user: "expo_root_user",
            password: "expo_root_pwd",
            database: "crawler_db"
        });
        
        con.connect(function (err) {
            if(err){
                console.log('Error connecting to DB: ', err);
            }
        });
        
        return con;
    }
    
    var disconnectDB = function (con) {
        
        con.end(function(err) {            
        });
    }
    
    me.addUrls = function (urls, status, keywordId) {
        
        if(!me.addUrlsCon)
            me.addUrlsCon = getDbConnection();
        
        // for bulk insert
        params = urls.map(function (url) {
            return [ url , (new Date).toMysqlFormat()];
        });
        
        runQuery(me.addUrlsCon, 'INSERT IGNORE INTO crawled_urls(url, crawled_at) VALUES ?', [params], function (err, res) {
            if(!err)
                console.log('ID:', res.insertId);
            else console.log(err);
        });
        
        if(status === true){
            var sql = 'UPDATE keywords SET till_google_page=till_google_page+10 WHERE pk_id=?';
            runQuery(me.addUrlsCon, sql, [keywordId], function (err, res) {
                if(!err)
                    console.log('till google page incremented');
                else console.log(err);
            });
            
            // keyword_urls table
            sql = 'INSERT IGNORE INTO keyword_urls (url_id, keyword_id) '   +
                  'SELECT u.pk_id, k.pk_id '                                +
                  'FROM crawled_urls u '          +
                  '    INNER JOIN keywords k on k.pk_id=' + keywordId + ' ' +
                  'WHERE u.url IN (?);';
            
            runQuery(me.addUrlsCon, sql, [urls], function (err, res) {
                if(err) console.log(err);
            });
        }
    }
    
    me.getKeywordsFromQueue = function (callback) {
        
        if(!me.keywordQueueCon)
            me.keywordQueueCon = getDbConnection();
            
        var sqlQuery = "SELECT pk_id, keyword, till_google_page " +
                       "FROM crawler_db.keywords k "              +
                       "WHERE k.is_in_queue = 1 ORDER BY pk_id desc";
                        
        runQuery(me.keywordQueueCon, sqlQuery, null, function (err, rows) {
            
            callback({
                err: err ? true : false,
                data: err ? null : rows
            });
        });
    }
    
    me.removeFromQueue = function (id) {
        
        if(!me.keywordRemCon)
            me.keywordRemCon = getDbConnection();
            
        var sqlQuery = "UPDATE keywords " +
                       "SET is_in_queue=0 "           +
                       "WHERE pk_id=" + id;
                       
        runQuery(me.keywordRemCon, sqlQuery, null, function (err, rows) {
            if(err)
                console.log(err);
        });
    };
    
    me.getUrlsForScraping = function () {
        
        if(!me.scrapUrlsCon)
            me.scrapUrlsCon = getDbConnection();
            
        var sqlQuery = "SELECT pk_id, url "                             +
                       "FROM crawled_urls "                             +
                       "WHERE is_scrapped not in (1,2) and length(url) > 0 "    +
                       "ORDER BY pk_id DESC LIMIT 25;";
                       
        return Q.Promise(function (resolve, reject, notify) {
            
            runQuery(me.scrapUrlsCon, sqlQuery, null, function (err, rows) {
                if(err){
                    reject(err);
                }else{
                    resolve(rows);
                }
            });
        });
        
    };
    
    me.getSearchPatterns = function () {
        
        if(!me.patternsCon)
            me.patternsCon = getDbConnection();
            
        var sqlQuery = "SELECT pk_id, pattern " +
                       "FROM search_patterns "  +
                       "WHERE is_enqueue = 1 ";
                       
        return Q.Promise(function (resolve, reject, notify) {
            
            runQuery(me.patternsCon, sqlQuery, null, function (err, rows) {
                if(err){
                    reject(err);
                }else{
                    resolve(rows);
                }
            });
        });
    }
    
    me.setScrapedFlag = function (urlId, flag) {
        if(!me.scrapCon)
            me.scrapCon = getDbConnection();
            
        var sqlQuery = 'UPDATE crawled_urls SET is_scrapped='+ flag +' WHERE pk_id='+urlId;
        
        runQuery(me.scrapCon, sqlQuery, null, function (err, res) {
            if(err) console.log(err);
        });
    }
    
    var addEmailAddresses = function (urlId, emails) {
        if(!me.emailAddressCon)
            me.emailAddressCon = getDbConnection();
            
        var sqlQuery = 'UPDATE crawled_urls SET emails = ? WHERE pk_id = ?;';
            
        runQuery(me.emailAddressCon, sqlQuery, [emails, urlId], function (err, res) {
            if(err)
                console.log(err);
        });
    }
    
    me.addUrlFoundPatterns = function (urlId, patternId, emails) {
        if(!me.addUrlsPattern)
            me.addUrlsPattern = getDbConnection();
            
        var sqlQuery = 'INSERT IGNORE INTO matched_patterns(pattern_id, url_id) VALUES (' + patternId + ',' + urlId + ');';
            
        runQuery(me.addUrlsPattern, sqlQuery, null, function (err, res) {
            if(!err){
                addEmailAddresses(urlId, emails);
                me.setScrapedFlag(urlId,1);
            }
            else console.log(err);
        });
    }
    
    var init = function () {       

    };
    
    init();
};

// module registartion
module.exports = {

    create: function () {
        return new MySqlLib();
    }

}

// Date lib
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};