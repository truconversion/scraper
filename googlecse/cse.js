

var GoogleCSE = function() {
    
    var me = this;
    
    // Configuration
    var config = {
        API_KEY: 'AIzaSyBOR4htgZ03LYCrVk5qqsEJ-_7nuYcy36s',
        cx: '016536472504692714701:0fi20z7bhdk'
    };

    var init = function () {
        
    };
    
    var extractDomain = function (url) {
        
        var queryString = url.replace('/url?', ''),
            queryStringValues = queryString.split('&'),
            domain,
            protocol = '';
        
        url = queryStringValues[0].replace('q=', '');
        
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            protocol = url.substr(0, url.indexOf("://") + 3);
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];
        
        // append protocol
        domain = protocol+domain;

        return domain.trim();
    }
    
    me.search = function (keyword, startIndex, callback) {
        
        var page = require('webpage').create();
        
        startIndex = startIndex || 1;
        console.log('=================================================================');
        console.log('called for keywrod: ', keyword, ' index: ', startIndex, ' at ', (new Date).toString());
        
        var searchUri = 'https://www.google.com/?gws_rd=cr&ei=yXYMV-K5K8rzUJ2_j-gE'+
                        '#q=' + keyword + 
                        '&start=' + startIndex;
        
        page.open(searchUri, function(status){
            
            if(status != 'success') {
                console.log('cse status ', page.plainText);
                callback({
                    success: false,
                    results: null,
                    hasMore: false
                });
                return;
            };
            
            console.log('page opened');
        
            var links = page.evaluate(function () {
                    
                var matches = document.querySelectorAll('h3.r a'),
                    nodes = [];
                    
                    if(!matches || matches.length == 0){
                        console.log('no matches found...');
                        
                    }
                    
                for(var i = 0; i < matches.length; ++i) {
                    nodes.push(matches[i].getAttribute('href'));
                }
                
                return nodes;
            });
            
            // var hasMorePages = page.evaluate(function () {
            //     var pnnextQuery = document.querySelectorAll('a.pn');        
            //     return pnnextQuery.length > 0;
            // });
            
            var results = [];
            
            if(!links || links.length == 0){
                console.log('no links found...');
            }else{
                console.log('total links are ', links.length);
            }
                
            links.forEach(function(link){
                console.log(extractDomain(link));
                results.push(extractDomain(link));
            });   
         
            console.log('calling callback');
            callback({
                success: true,
                results: results
            });
        });
    }
};

module.exports = {

    create: function () {
        return new GoogleCSE();
    }

}