var request = require('request');
var mysql = require('./db/mysqllib').create();
var scraper = require('./scraper').create();
var util = require('util');
var Q = require('q');

var Processor = function () {
    
    var me = this;
    me.delay = 0;
    
    var addUrlsInDatabase = function (urls) {
        
        mysql.addUrls(urls.results, urls.success, urls.keywordsId);
    };
    
    var removeKeywordFromQueue = function (id) {
        if(mysql.removeFromQueue)
            mysql.removeFromQueue(id);
        else console.log('ohhhhh');
    }
    
    var crawlKeyword = function (keyword, resolve, reject) {
        
        console.log(util.format('start crawling \"%s\" at %s', keyword.text, (new Date).toMysqlFormat()));
        
        // use a timeout value of 60 seconds
        var timeoutInMilliseconds = 1*60*1000,
            //
            opts = {
                url: 'http://ec2-52-23-225-132.compute-1.amazonaws.com:8080/?keyword=' + keyword.text + '&pageNumber='+keyword.start,
                timeout: timeoutInMilliseconds
            };
            
        try {
        
            request(opts, function (err, res, body) {
                if (err) {
                    console.log('errrrrr');
                    console.log(err);
                    reject(err);
                    return;
                }
                var statusCode = res.statusCode;
                console.log('status code: ', statusCode);
                
                if(statusCode == 200){
                    
                    var data = JSON.parse(body);
                    
                    //console.log(body);
                    data.keywordsId = keyword.id;
                    if(data && data.results && data.results.length > 0)
                        addUrlsInDatabase(data);
                    else
                        removeKeywordFromQueue(keyword.id);
                }
                
                resolve('done');
            });
        } catch (error) {
            console.log('try: ',error);
        }
        
    }
    
    var searchPatternInPage = function (urlObj, patterns) {
        
        var jobs= [];
        console.log('start searching in ', urlObj.url);
        patterns.forEach(function (p) {
            
            //console.log('pattern is in ', p.pattern);
            var shortUrl = ''; // url without protocol and www
            var job = scraper.searchInPage(urlObj.url, p.pattern)
                   .then(function (result) {
                       
                       if(result && result.success === true && result.matches.length > 0){
                           console.log('url: ', urlObj.url, result);
                           //console.log('patterns: ', result);
                           mysql.addUrlFoundPatterns(urlObj.pk_id, p.pk_id, result.emails);
                       }else if(result && result.success === true && result.matches.length == 0){
                           mysql.setScrapedFlag(urlObj.pk_id, 1);
                           console.log('url: ', urlObj.url, ' matches: ', 'NO MATCHES FOUND');
                       }else{
                           mysql.setScrapedFlag(urlObj.pk_id, 2);
                           console.log('url: ', urlObj.url, ' matches: ', 'ERROR occurred');
                       }
                   }).done();
        });
        
    };
    
    me.run = function () {
        
        //get keywords for crawling
        mysql.getKeywordsFromQueue(function (result) {
            
            if(result && result.err){
                console.log('aaah, something went wrong!');
                return;
            }
            
            if(result && result.data && result.data.length > 0){
                
                var promises = [];
                
                var delay = 0;
                
                result.data.forEach(function (record) {
                    
                    //console.log(util.format('%s will be start after %d seconds', record.keyword, delay));
                    
                    var p = Q.Promise(function (resolve, reject, notify) {
                        
                        var carwlInput = {
                            id: record.pk_id, 
                            text: record.keyword, 
                            start: record.till_google_page
                        };
                        setTimeout(crawlKeyword, delay*1000, carwlInput, resolve, reject);
                    });
                    
                    promises.push(p);
                    
                    // get a random value between 60(0+60) and 120(60+60) seconds
                    delay = delay + Math.floor((Math.random() * 60) + 60);
                    console.log(util.format('%s will start after %d seconds', record.keyword, delay));
                });
                
                Q.all(promises)
                .then(function () {
                    setTimeout(me.run, delay*1000);
                })
                .done();
                
            }else{
                console.log('No keyword in Queue!');
                setTimeout(me.run, 1*60*1000);
            }            
        });

    };
    
    me.startScraping = function () {
        
        // get URLs and search patterns for scraping
        Q.allSettled([mysql.getUrlsForScraping(), mysql.getSearchPatterns()])
        .spread(function (urls, patterns) {
            
            urls.value.forEach(function (url) {
                searchPatternInPage(url, patterns.value);
            });
        });
        
        setTimeout(me.startScraping, 1*60*1000); // run after 1 minute
    }
    
    var init = function () {
        
    };
    
    init();
};


var p = new Processor();
p.run();
p.startScraping();